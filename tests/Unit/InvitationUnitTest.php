<?php


namespace Tests\Unit;


use App\Models\Invitation;
use App\Models\Status;
use App\Models\User;
use App\Services\InvitationService;
use Illuminate\Validation\ValidationException;
use Mockery;
use Tests\Setup;

class InvitationUnitTest extends Setup
{
    private $mockInvitationModel;

    private $defaultInvitation;

    public function setUp(): void
    {
        parent::setUp();
        $this->mockInvitationModel = Mockery::mock(Invitation::class);
        $this->defaultInvitation = factory(Invitation::class)->create();
    }

    public function testCreateInvitationService()
    {
        $sender = factory(User::class)->create();
        $invited = factory(User::class)->create();

        $this->mockInvitationModel->allows([
            "create" => $this->defaultInvitation,
        ]);

        $service = new InvitationService($this->mockInvitationModel);
        $invitation = $service->createInvitation($sender->id, $invited->id);

        $this->assertEquals($this->defaultInvitation, $invitation);
        $this->assertInstanceOf(Invitation::class, $invitation);
    }

    public function testUpdateInvitationStatusService()
    {
        $invitation = factory(Invitation::class)->create();
        $service = new InvitationService($this->mockInvitationModel);
        $service->updateInvitationStatus($invitation->id, Status::CANCELED);
        $this->assertInstanceOf(Invitation::class, $invitation);
    }
}
