<?php
namespace Tests;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Mockery;

class Setup extends TestCase
{
    use DatabaseTransactions;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @after
     */
    public function tearDown(): void
    {
        Mockery::close();
        DB::rollBack();
        DB::disconnect();
    }

    /**
     * Init any model data
     *
     * @param string $class
     * @param int $quantity
     * @return void
     */
    public function initDefaultData(string $class, int $quantity = 5): void
    {
        factory($class, $quantity)->create();
    }
}
