<?php


namespace Tests\Integration;

use App\Models\Invitation;
use App\Models\Status;
use App\Models\User;
use Tests\Setup;

class InvitationIntegrationTest extends Setup
{
    /**
     * structure invitation const
     */
    protected const STRUCTUREINVITATION = [
        'id',
        'sender_id',
        'invited_id',
        'status_id'
    ];

    public $invitation;

    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->invitation = factory(Invitation::class)->create();
    }

    public function testCreateInvitation()
    {
        $invitedUser = factory(User::class)->create();
        $data = [
            "invited_id" => $invitedUser->id
        ];
        $response = $this->post("api/sender/".$this->user->id."/invitation", $data);

        $response->assertStatus(200);
        $response->assertJsonStructure(self::STRUCTUREINVITATION);
        $response->assertJsonFragment($data);
    }

    public function testCancelInvitation()
    {
        $data = [
            "status_id" => Status::CANCELED
        ];
        $response = $this->put("api/sender/".$this->user->id."/invitation/".$this->invitation->id, $data);

        $response->assertStatus(200);
        $response->assertJsonStructure(self::STRUCTUREINVITATION);
        $response->assertJsonFragment($data);
    }

    public function testAcceptInvitation()
    {
        $data = [
            "status_id" => Status::ACCEPTED
        ];
        $response = $this->put("api/invited/".$this->invitation->invited_id."/invitation/".$this->invitation->id, $data);

        $response->assertStatus(200);
        $response->assertJsonStructure(self::STRUCTUREINVITATION);
        $response->assertJsonFragment($data);
    }

    public function testRejectInvitation()
    {
        $data = [
            "status_id" => Status::REJECTED
        ];
        $response = $this->put("api/invited/".$this->invitation->invited_id."/invitation/".$this->invitation->id, $data);

        $response->assertStatus(200);
        $response->assertJsonStructure(self::STRUCTUREINVITATION);
        $response->assertJsonFragment($data);
    }

    public function testCreateInvitationWithoutInvitedId()
    {
        $response = $this->post("api/sender/".$this->user->id."/invitation", []);
        $response->assertStatus(422);
        $response->assertJsonStructure(["invited_id"]);
        $this->assertContains("must be sent", $response->getContent());
    }

    public function testCancelInvitationWithWrongStatus()
    {
        $data = [
            "status_id" => Status::REJECTED
        ];
        $response = $this->put("api/sender/".$this->user->id."/invitation/".$this->invitation->id, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(["status_id"]);
        $this->assertContains("status_id must be", $response->getContent());
    }
}
