<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    const SENT = 1;
    const ACCEPTED = 2;
    const REJECTED = 3;
    const CANCELED = 4;

    protected $primaryKey = 'id';
    protected $table = 'statuses';

    protected $fillable = [
        'id',
        'status'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

}
