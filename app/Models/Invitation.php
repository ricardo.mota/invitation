<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invitation extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'invitations';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id',
        'sender_id',
        'invited_id',
        'status_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

}
