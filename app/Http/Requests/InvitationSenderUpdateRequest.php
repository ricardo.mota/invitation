<?php

namespace App\Http\Requests;

use App\Rules\InvitationSenderUpdateRule;
use App\Rules\InvitationInvitedUpdateRule;
use Illuminate\Foundation\Http\FormRequest;

class InvitationSenderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'status_id' => [
                new InvitationSenderUpdateRule,
                'integer',
                'required'
            ]
        ];
        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'status_id.integer' => '[:attribute] - must be an integer',
            'status_id.required' => '[:attribute] - must be sent'
        ];
    }
}
