<?php

namespace App\Http\Controllers;

use App\Http\Requests\InvitationSenderUpdateRequest;
use App\Http\Requests\InvitationInvitedUpdateRequest;
use App\Http\Requests\InvitationStoreRequest;
use App\Services\InvitationService;
use Illuminate\Http\JsonResponse;

class InvitationController extends Controller
{
    private $invitationService;

    /**
     * Constructor class
     *
     * @access public
     * @param InvitationService $invitationService
     * @return void
     */
    public function __construct(InvitationService $invitationService)
    {
        $this->invitationService = $invitationService;
    }

    /**
     * @param InvitationStoreRequest $request
     * @param $userId
     * @return JsonResponse
     */
    public function store(InvitationStoreRequest $request, $userId): JsonResponse
    {
        $invitation = $this->invitationService->createInvitation($userId, $request->invited_id);
        return response()->json($invitation);
    }

    /**
     * @param InvitationSenderUpdateRequest $request
     * @param $userId
     * @param $invitationId
     * @return JsonResponse
     */
    public function senderUpdate(InvitationSenderUpdateRequest $request, $userId, $invitationId): JsonResponse
    {
        $invitation = $this->invitationService->updateInvitationStatus($invitationId, $request->status_id);
        return response()->json($invitation);
    }

    /**
     * @param InvitationInvitedUpdateRequest $request
     * @param $userId
     * @param $invitationId
     * @return JsonResponse
     */
    public function invitedUpdate(InvitationInvitedUpdateRequest $request, $userId, $invitationId): JsonResponse
    {
        $invitation = $this->invitationService->updateInvitationStatus($invitationId, $request->status_id);
        return response()->json($invitation);
    }
}
