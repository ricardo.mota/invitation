<?php


namespace App\Services;

use App\Models\Invitation;
use App\Models\Status;

class InvitationService
{
    /**
     * @var Invitation
     */
    public $invitationModel;

    /**
     * InvitationService constructor.
     * @param Invitation $invitation
     */
    public function __construct(Invitation $invitation)
    {
        $this->invitationModel = $invitation;
    }

    /**
     * @param int $senderId
     * @param int $invitedId
     * @return Invitation
     */
    public function createInvitation(int $senderId, int $invitedId): Invitation
    {
        $data = [
            'sender_id' => $senderId,
            'invited_id' => $invitedId,
            'status_id' => Status::SENT
        ];
        $invitation = $this->invitationModel::create($data);

        return $invitation;
    }

    /**
     * @param int $invitationId
     * @param int $statusId
     * @return Invitation
     */
    public function updateInvitationStatus(int $invitationId, int $statusId): Invitation
    {
        $invitation = Invitation::find($invitationId);
        $invitation->status_id = $statusId;
        $invitation->save();

        return $invitation;
    }
}
