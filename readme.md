# API Invitation

### Installation

This API requires [PHP](http://www.php.net/) v7.1+ to run.

Clone the project:
```sh
$ https://gitlab.com/ricardo.mota/invitation.git
```

Go to folder:
```sh
$ cd invitation
```

Install the dependencies:
```sh
$ composer install
```

Create your .env:
```sh
$ cp .env.example .env
```

Configure your .env:
```sh
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE={YOUR_NAME_DATABASE}
DB_USERNAME={YOUR_USERNAME}
DB_PASSWORD={YOUR_PASSWORD}
```

Run the migrates and seed:
```sh
$ php artisan migrate --seed
```
### API Resources


| Method | URI | Description |
| ------ | ------ | ------ |
| POST | [/api/sender/{userId}/invitation](#create-invitation) | Create Invitation |
| PUT | [/api/sender/{userId}/invitation/{invitationId}](#cancel-invitation) | Cancel Invitation |
| PUT | [/api/invited/{userId}/invitation/{invitationId}](#accept-reject-invitation) | Accept or Reject Invitation |

#### Invitation fields:

| Name | Type | Description |
| ------ | ------ | ------ |
| id | integer | Invitation id |
| sender_id | integer | Sender id |
| invited_id | integer | Invited id |
| status_id | integer | Status Id |

#### Invitation Statuses:

| Name | id |
| ------ | ------ |
| SENT | 1 |
| ACCEPTED | 2 |
| REJECTED | 3 |
| CANCELED | 4 |

### POST /api/sender/{userId}/invitation

Example: /api/sender/2/invitation

Request body Example:

     {
         "invited_id": 2,
     }

Response body Success:

Status Code: 200

     {
         "id": 370,
         "sender_id": 2,
         "invited_id": 2,
         "status_id": 1
     }


### PUT /api/sender/{userId}/invitation/{invitationId}

Example: /api/sender/1/invitation/7

Request body Example:

     {
         "status_id": 4,
     }

Response body Success:

Status Code: 200

     {
         "id": 7,
         "sender_id": 1,
         "invited_id": 2,
         "status_id": 4
     }


### PUT /api/invited/{userId}/invitation/{invitationId}

Example: /api/invited/2/invitation/7

Request body Example:

     {
         "status_id": 2,
     }

Response body Success:

Status Code: 200

     {
         "id": 7,
         "sender_id": 1,
         "invited_id": 2,
         "status_id": 2
     }
