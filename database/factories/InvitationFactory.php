<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Invitation;
use App\Models\Status;
use App\Models\User;
use Faker\Generator as Faker;



$factory->define(Invitation::class, function (Faker $faker) {
    return [
        'sender_id' => factory(User::class)->create(),
        'invited_id' => factory(User::class)->create(),
        'status_id' => Status::inRandomOrder()->first()
    ];
});
