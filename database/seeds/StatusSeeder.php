<?php

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusSeeder extends Seeder
{
    const STATUSES = [
        ['id' => Status::SENT, 'status' => 'SENT'],
        ['id' => Status::ACCEPTED, 'status' => 'ACCEPTED'],
        ['id' => Status::REJECTED, 'status' => 'REJECTED'],
        ['id' => Status::CANCELED, 'status' => 'CANCELED']
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::insert(
            self::STATUSES
        );
    }
}
