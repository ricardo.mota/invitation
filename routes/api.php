<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/user/{userId}/invitation', 'InvitationController@index');
Route::post('/sender/{userId}/invitation', 'InvitationController@store');
Route::put('/sender/{userId}/invitation/{invitationId}', 'InvitationController@senderUpdate');
Route::put('/invited/{userId}/invitation/{invitationId}', 'InvitationController@invitedUpdate');

